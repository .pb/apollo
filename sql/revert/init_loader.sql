-- Revert init_loader

BEGIN;

DROP TRIGGER IF EXISTS apollo_reload_on_module_update ON modules;
DROP TRIGGER IF EXISTS apollo_default_symbol ON autoloads;

DROP FUNCTION IF EXISTS trig_auto_default_symbol();
DROP FUNCTION IF EXISTS initialise();
DROP FUNCTION IF EXISTS install();
DROP FUNCTION IF EXISTS reload();
DROP TABLE IF EXISTS autoloads;
DROP TABLE IF EXISTS modules;
DROP EXTENSION plv8;

COMMIT;
