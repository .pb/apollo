-- Deploy init_loader

BEGIN;

CREATE EXTENSION IF NOT EXISTS plv8;

CREATE TABLE modules (
    ident       text PRIMARY KEY,
    definition  text,
    manifest    jsonb DEFAULT '{}',
    enabled     bool DEFAULT TRUE,
    created_at  timestamp DEFAULT current_timestamp
);

CREATE TABLE autoloads (
    ident   text PRIMARY KEY,
    symbol  text UNIQUE,
    enabled bool DEFAULT TRUE,
    FOREIGN KEY (ident) REFERENCES modules(ident) ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION trig_auto_default_symbol() RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.symbol IS NULL) THEN
        NEW.symbol := NEW.ident;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql IMMUTABLE;

CREATE TRIGGER apollo_default_symbol BEFORE INSERT
ON autoloads FOR EACH ROW EXECUTE PROCEDURE trig_auto_default_symbol();

CREATE OR REPLACE FUNCTION install() RETURNS VOID AS $$
DECLARE
    workSchema text;
    initProc   text;
BEGIN
    /*
     * The concatenated string is replaced when we install as a PostgreSQL extension.
     * In which case, the replacement is the schema into which we are installing.
     * If no replacement occurs, then we're not installing via an extension, and we should
     * look up the current schema instead. Note that the replaced version and current_schema
     * will be identical if install() is called during extension installation, but if called
     * manually then current_schema may not be correct.
     */
    IF '@extschema@' = '@' || 'extschema@' THEN
        workSchema := current_schema;
    ELSE
        workSchema := '@extschema@';
    END IF;

    initProc := workSchema || '.initialise';
    PERFORM initialise();
    EXECUTE $ex$
        ALTER DATABASE $ex$ || current_database() || $ex$
            SET plv8.start_proc = '$ex$ || initProc || $ex$';
    $ex$;

    RAISE NOTICE 'Installation complete!';
END
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION initialise() RETURNS VOID AS $js$ // @begin=js@
const getSchema = function() {
    if ('@extschema@' == '@' + 'extschema@')
        return plv8.execute('SELECT current_schema')[0].current_schema;

    return '@extschema@';
};

const quoteSchema = plv8.quote_ident(getSchema());

const canonicaliseIdent = function(ident, relative) {
    const identPath = ident.split('/');
    const procSegment = function(segment) {
    };

    var pathStack = [];
    var init      = true;
    var pathPtr   = 0;
    var seg;
    while (true) {
        seg = identPath[pathPtr++];

        if (init) {
            /* Relative paths work from the current 'directory', but
             * relative contains the current full path. Remove the module. */
            if (seg == '.' || seg == '..')
                pathStack = relative.slice(0, -1);

            init = false;
        }

        if (seg == undefined) {
            return {
                stack: pathStack,
                str:   pathStack.join('/'),
            };
        }

        if (seg == '..')
            pathStack.pop();
        else if (seg != '.')
            pathStack.push(seg);
    }
};

const getDefinition = function(ident) {
    const defRowArray = plv8.execute('SELECT definition FROM ' +
                                     quoteSchema + '.modules ' +
                                     'WHERE ident = $1 AND enabled = true',
                                     [ident]);

    if (!defRowArray.length)
        throw "Couldn't resolve request for module " + ident;

    return defRowArray[0].definition;
};

const buildSandbox = function(ident, definition) {
    var module = new Function('module', 'exports', 'require', definition);
    module.id      = ident;
    module.exports = {};

    return module;
};

const req = function(relative, ident) {
    if (!ident)
        throw 'require(): Missing ident!';

    if (!('moduleCache' in req))
        req.moduleCache = {};

    const id = canonicaliseIdent(ident, relative);

    if (id.str in req.moduleCache)
        return req.moduleCache[id.str].exports;

    req.moduleCache[id.str] = buildSandbox(id.str, getDefinition(id.str));
    var m = req.moduleCache[id.str];
    m(m, m.exports, req.bind(undefined, id.stack)); // Module, Exports, Require

    return m.exports;
};

const doAutoloads = function() {
    plv8.execute('SELECT ident, symbol FROM ' + quoteSchema +
                 '.autoloads WHERE enabled = TRUE;').forEach((function(row) {
        this[row.symbol] = this.require(row.ident);
    }).bind(this));
};

this.require      = req.bind(undefined, []);
this.require.root = req;
this.require.auto = doAutoloads.bind(this);
this.require.auto();
// @end=js@
$js$  LANGUAGE plv8 VOLATILE;

CREATE OR REPLACE FUNCTION reload() RETURNS trigger AS $js$ // @begin=js@
    delete require.root.moduleCache;
    require.auto();
    return null;
// @end=js@
$js$  LANGUAGE plv8 VOLATILE;

CREATE TRIGGER apollo_reload_on_module_update AFTER UPDATE OF definition ON modules
FOR EACH STATEMENT EXECUTE PROCEDURE reload();

SELECT install();

COMMIT;
