-- Verify init_loader

BEGIN;

SELECT ident, definition, manifest, enabled, created_at FROM modules WHERE FALSE;
SELECT 'install'::regproc, 'initialise'::regproc, 'reload'::regproc, 'trig_auto_default_symbol'::regproc;
SHOW plv8.start_proc;
SELECT 1/count(*) FROM pg_trigger t WHERE NOT tgisinternal AND tgrelid = 'modules'::regclass AND tgname = 'apollo_reload_on_module_update';
SELECT 1/count(*) FROM pg_trigger t WHERE NOT tgisinternal AND tgrelid = 'autoloads'::regclass AND tgname = 'apollo_default_symbol';

ROLLBACK;
