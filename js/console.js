const eLevel = {
    ERROR:  0,
    WARN:   1,
    INFO:   2,
    DEBUG:  3,
    ASSERT: 4,
};

const ERROR_MAX_LEN = 95;

const formatMsg = function(message, params) {
    const regex     = /%([sdifoO0c])/g;
    var   paramPtr  = 0;
    var   formatted = String(message).replace(regex, function(match, spec) {
        var p = params[paramPtr++];

        switch (spec) {
            case 'd':
            case 'i':
                return parseInt(p);

            case 'f':
                return parseFloat(p);

            case 'o':
            case 'O':
            case '0':
                var wsp = 1;
                if ('length' in p && p.length < 4)
                    wsp = 0;

                return JSON.stringify(p, null, wsp);

            case 'c':
            case 's':
            default:
                return String(p);
        }
    });

    if (params && paramPtr < params.length)
        formatted += ' ' + params.slice(paramPtr).join(' ');

    return formatted;
};

const displayError = function(level, trace, message, params) {
    const describe = function(level) {
        switch (level) {
            case eLevel.ERROR:  return "v8-ERR ";
            case eLevel.WARN:   return "v8-Warn";
            case eLevel.INFO:   return "v8-Info";
            case eLevel.DEBUG:  return "v8-dbg ";
            case eLevel.ASSERT: return "v8-ASSERT FAIL";
            default: return "";
        }
    };

    const plv8Level = function(level) {
        switch (level) {
            case eLevel.ERROR:  return ERROR;
            case eLevel.WARN:   return WARNING;
            case eLevel.INFO:   return INFO;
            case eLevel.DEBUG:  return LOG;
            case eLevel.ASSERT: return ERROR;
            default: throw "Can't resolve plv8 level for " + level;
        }
    };

    const typeSpace = function(level) {
        switch (level) {
            case eLevel.ERROR:  return '  ';
            case eLevel.WARN:   return '';
            case eLevel.INFO:   return '   ';
            case eLevel.DEBUG:  return '    ';
            case eLevel.ASSERT: return '  ';
            default: throw "Can't resolve plv8 level for " + level;
        }
    }

    var output = formatMsg(message, params);

    if (trace) {
        var traceObj = {};
        var stackSrc = typeof trace == 'function' ? trace : undefined;
        Error.captureStackTrace(traceObj, stackSrc);
        output += "\nStacktrace: " + traceObj.stack;
    }

    var groupStr = "";

    for (var i = groupStack.length; i > 0; i--)
        groupStr += "| ";

    const outStr       = typeSpace(level) + describe(level) + ": "
                       + groupStr + output.replace("\n", "\n" + groupStr);
    var   errorStrings = [];

    /* plv8.elog seems to have some weird problem with long error strings.
     * Chunk it up for optimal output... */
    if (outStr.length > ERROR_MAX_LEN)
    {
        var start    = 0;
        const chunks = Math.ceil(outStr.length / ERROR_MAX_LEN);
        const len    = ERROR_MAX_LEN;
        for (var i = 0; i < chunks; i++) {
            errorStrings.push(outStr.substr(start, len));
            start  += ERROR_MAX_LEN;
        }
    }
    else
    {
        errorStrings.push(outStr);
    }

    const plv8lvl = plv8Level(level);

    errorStrings.forEach(function(str) {
        plv8.elog(plv8lvl, str);
    });
};

const error = function(object) {
    const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
    displayError(eLevel.ERROR, error, object, params);
};

const group = function(object) {
    const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
    const name = object ? object : '(unnamed)';
    displayError(eLevel.INFO, null, "/== Group [" + name + "] ============");
    groupStack.push(name);
    if (params)
        displayError(eLevel.INFO, null, "", params);
};

const log = function(object) {
    const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
    displayError(eLevel.DEBUG, null, object, params);
};

const dir = function(object) {
    log("%0", object);
};

const trace = function(object) {
    const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
    displayError(eLevel.DEBUG, trace)
};
const assert = function(expression, object) {
    if (!expression)
        displayError(eLevel.ASSERT, module.exports.assert, object);
};

var groupStack = [];
var counts     = {};

module.exports = {
    assert: assert,
    clear: function() {
        displayError(eLevel.INFO, null, "\033[2J\033[;H");
    },
    count: function(label) {
        const name = label ? label : 'anonymous';

        if (!(name in counts))
            counts[name] = 0;

        log("Count: %s: %i", label, ++counts[name]);
    },
    debug: log,
    dir: dir,
    dirxml: dir,
    error: error,
    exception: error,
    group: group,
    groupCollapsed: group,
    groupEnd: function() {
        const gName = groupStack.pop();

        if (gName !== undefined)
            displayError(eLevel.INFO, null, "\\== End [" + gName + "] ============");
    },
    info: function(object) {
        const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
        displayError(eLevel.INFO, null, object, params);
    },
    log: log,
    table: function(data, columns) {
        if (!(data.constructor === Array))
            return log(data);

        var rows      = 0;
        var cols      = 0;
        var colData   = {};
        var colWidths = {};

        data.forEach(function (row, index) {
            var filtered = {}, rowData = false;

            if (row === null) return;
            else if (row.constructor === Array) {
                row.forEach(function(rowData, idx) {
                    if (!columns || -1 !== columns.indexOf(idx))
                        filtered[idx] = rowData;
                });
            } else if (typeof row === 'object') {
                Object.keys(row).forEach(function(key) {
                    if (!columns || -1 !== columns.indexOf(key))
                        filtered[key] = row[key];
                });
            } else return;

            Object.keys(filtered).forEach(function (key) {
                rowData = true;
                if (!(key in colData)) {
                    colData[key]   = [];
                    colWidths[key] = key.length;
                    cols++;
                }

                colData[key][index] = String(filtered[key]);
                colWidths[key]      = Math.max(colWidths[key], colData[key][index].length);
            });

            if (rowData)
                rows++;
        });

        if (!rows)
            return log(data);

        const indexWidth = Math.max("idx".length, String(rows).length);
        var   colWidth   = 0;
        var   hdr        = "| idx |";
        const pad        = function(str, width) {
            return str + Array(Math.max(0, width - String(str).length) + 1).join(' ');
        };

        Object.keys(colWidths).forEach(function(key) {
            colWidth += colWidths[key];
            hdr      += ' ' + pad(key, colWidths[key]) + ' |';
        });

        log(hdr);

        const rowWidth = (cols + 1) * 4 + indexWidth + colWidth;
        log(Array(rowWidth).join('-'));

        for (var i = 0; i < rows; i++) {
            var rowData = "| " + pad(i, indexWidth) + " |";
            Object.keys(colData).forEach(function(key) {
                const data = colData[key][i];
                rowData += ' ' + pad(data === undefined ? '' : data, colWidths[key]) + ' |';
            });

            log(rowData);
        }
    },
    trace: trace,
    warn: function(object) {
        const params = arguments.length > 1 ? Array.apply(null, arguments).slice(1) : null;
        displayError(eLevel.WARN, null, object, params);
    },
};
